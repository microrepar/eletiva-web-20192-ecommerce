from app.bp_main import bp
from flask import render_template, request
from ew20192_core.utils import get_parameters_request, get_chain_of_filters


@bp.before_app_request
def filtro_request():
    """TODO:[summary]

    Returns:
        [type] -- [description]
    """    
    from app.bp_main import mapa_filtros as filtros
    uri = request.path
    filtro = get_chain_of_filters(uri, filtros)
    resultado = filtro.do_chain(request)
    if resultado:
        return resultado


@bp.route("/", methods=["GET"])
@bp.route("/index", methods=["GET"])
def index():
    return render_template("index.html")


@bp.route("/blog")
def blog():
    return render_template("blog.html")


@bp.route("/cart")
def cart():
    return render_template("cart.html")


@bp.route("/categories")
def categories():
    return render_template("categories.html")


@bp.route("/checkout")
def checkout():
    return render_template("checkout.html")


@bp.route("/product")
def product():
    return render_template("product.html")



