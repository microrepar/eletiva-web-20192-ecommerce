import jinja2
from flask import Blueprint, Flask
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from config import Config

db = SQLAlchemy()
migrate = Migrate()

login = LoginManager()
login.login_view = 'bp_admin.login'
login.login_message = 'O login é necessário para acessar esta página'



def create_app(config_class=Config):

    app = MyApp()

    app.config.from_object(config_class)

    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)

    from app.bp_main import bp as main_bp
    from app.bp_admin import bp as admin_bp
    from app.bp_user import bp as user_bp
    from app.bp_errors import bp as errors_bp

    app.register_blueprint(errors_bp)
    app.register_blueprint(admin_bp)
    app.register_blueprint(user_bp)
    app.register_blueprint(main_bp)

    return app


class MyApp(Flask):
    def __init__(self):
        Flask.__init__(self, __name__)
        self.jinja_loader = jinja2.ChoiceLoader(
            [self.jinja_loader, jinja2.PrefixLoader({}, delimiter=".")]
        )

    def create_global_jinja_loader(self):
        return self.jinja_loader

    def register_blueprint(self, bp: Blueprint):
        Flask.register_blueprint(self, bp)
        self.jinja_loader.loaders[1].mapping[bp.name] = bp.jinja_loader
