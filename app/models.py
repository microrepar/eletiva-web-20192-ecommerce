import datetime

from flask_login import UserMixin
from sqlalchemy import (Boolean, Column, Date, DateTime, ForeignKey, Integer,
                        String, Text)
from sqlalchemy.orm import relationship
from werkzeug.security import check_password_hash, generate_password_hash

from app import db, login




class Usuario(UserMixin, db.Model):

    __tablename__ = 'tb_usuario'

    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True)
    email = Column(String(120), index=True, unique=True)
    perfil = Column(String(50))
    password_hash = Column(String(128))
    last_seen = Column(DateTime, default=datetime.datetime.utcnow)
    about_me = Column(String(140))

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):
    return Usuario.query.get(int(id))
