from flask import Blueprint

bp = Blueprint('bp_errors', __name__, template_folder='templates')

from app.bp_errors import handlers