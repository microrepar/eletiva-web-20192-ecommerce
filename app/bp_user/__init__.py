from flask import Blueprint

bp = Blueprint('bp_user', __name__, url_prefix='/user', template_folder='templates', static_folder='static')

from app.bp_user import routes, models, viewhelper