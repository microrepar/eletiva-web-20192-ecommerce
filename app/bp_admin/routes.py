from app.models import Usuario
from app.bp_admin import bp
from flask import render_template, request, redirect, url_for, flash
from flask_login import current_user, login_user, logout_user
from werkzeug.urls import url_parse

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('bp_admin.index.html')


@bp.route('/register', methods=['GET', 'POST'])
def register():
    return render_template('bp_admin.register.html')

@bp.route('/forgot_password', methods=['GET', 'POST'])
def forgot_password():
    return render_template('bp_admin.forgot_password.html')

@bp.route('/page_404', methods=['GET', 'POST'])
def page_404():
    return render_template('bp_admin.page_404.html')

@bp.route('/blank', methods=['GET', 'POST'])
def blank():
    return render_template('bp_admin.blank.html')

@bp.route('/charts', methods=['GET', 'POST'])
def charts():
    return render_template('bp_admin.charts.html')

@bp.route('/tables', methods=['GET', 'POST'])
def tables():
    return render_template('bp_admin.tables.html')

@bp.route('/login', methods=['GET', 'POST'])
def login():
    username = request.form.get('username')
    if current_user.is_authenticated and current_user.perfil == 'admin':
        return redirect(url_for('bp_admin.index'))
    elif current_user.is_authenticated and current_user.perfil != 'admin':
        return redirect(url_for('bp_main.index'))    
    if request.method == 'POST':              
        email = request.form.get('email')
        senha = request.form.get('password')
        remember = request.form.get('remember_me')
        usuario = Usuario.query.filter_by(email=email).first()
        if usuario is None or not usuario.check_password(senha):
            flash('Nome de usuário ou senha inválido!')
            return render_template('bp_admin.login.html', resultado=request)
        login_user(usuario, remember=remember)
        next_page = request.form.get('next')
        print('>>>>>nextpage>>>>>', next_page)
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('bp_main.index')
        return redirect(next_page)
    return render_template('bp_admin.login.html', resultado=request)

@bp.route('/base', methods=['GET', 'POST'])
def base():
    return render_template('bp_admin.base.html')

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('bp_main.index'))