from flask import Blueprint

bp = Blueprint('bp_admin', __name__, url_prefix='/admin', static_folder='static', template_folder='templates')

from app.bp_admin import routes, models