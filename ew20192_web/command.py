from ew20192_core.fachada import Fachada
from ew20192_core.aplicacao import Resultado


commands = {}


def command(operacao=None):
    """Efetua o mapeamento das commands indexando pelo nome da operacao da requisicao
    
    Keyword Arguments:
        operacao {str} -- nome da operacao enviada na requisicao (default: {None})
    
    Returns:
        decorador -- funcao decoradora responsavel pelo mapeamento em tempo de importacao
    """
    def wrapper(command_func):      
        commands.setdefault(operacao, command_func)
        return command_func
    return wrapper


@command('SALVAR')
def salvar_command(entidade):
    fachada = Fachada()
    return fachada.salvar(entidade)



@command('ATUALIZAR')
def atualizar_command(entidade):
    fachada = Fachada()
    return fachada.atualizar(entidade)


@command('LISTAR')
def listar_command(entidade):
    fachada = Fachada()
    return fachada.listar(entidade)


@command(None)
@command('PRE-ABRIR-LIVRO')
def consultar_command(entidade):
    fachada = Fachada()
    return fachada.consultar(entidade)


@command('PRE-ATUALIZAR')
@command('ABRIR-LIVRO')
@command('PRE-EXCLUIR')
def consultar_por_id(entidade):
    fachada = Fachada()
    return fachada.consultarPorId(entidade)


@command('EXCLUIR')
def excluir_command(entidade):
    fachada = Fachada()
    return fachada.excluir(entidade)


@command('PRE-OPERACAO-ATUALIZAR')
@command('PRE-SALVAR')
def pre_operacao_command(entidade):
    fachada = Fachada()
    return fachada.preOperacao(entidade)


@command('SERVIR')     
@command('GERAR')        
def servico_command(entidade):
    fachada = Fachada()
    return fachada.executar_servico(entidade)


