"""[summary]
"""
import abc
from flask import flash



class AbstractViewHelper(abc.ABC):
    @abc.abstractmethod
    def get_entidade(self, request):
        """Recebe uma requisição e retorna uma instância do objeto com os dados preenchidos
        :rtype:
        """

    @abc.abstractmethod
    def set_view(self, resultado, request):
        """Recebe o objeto resultado da requisição e prepara para apresentar na view para o cliente
        """


class ErrorNotFoundViewHelper(AbstractViewHelper):
    def get_entidade(self, entidade):
        from flask import abort
        abort(404, description="Resource not found")

    def set_view(self, resultado, request):
        ...


# Função para verificar se há mensagens de erro e carrega-las na flash função
def _verificar_msg(resultado: "PresentationModel") -> bool:
    if resultado.qtde_msg() > 0:
        for msg in resultado.msg:
            flash(msg)
        return True
    return False


# dicionario de viewhelpers para mapeamento dos recursos
viewhelpers = {None: ErrorNotFoundViewHelper}


def _map_recursos(recurso=None):
    """Efetua o mapeamento das viewhepers mapeando-as pelo nome do recurso
    
    Keyword Arguments:
        recurso {str} -- nome do recurso recuperado da uri da requisicao(default: {None})
    
    Returns:
        wrapper {function} -- retorna a funcao decoradora responsavel pelo mapeamento em tempo de importacao
    """

    def wrapper(cls):
        viewhelpers.update({recurso: cls})
        return cls

    return wrapper
