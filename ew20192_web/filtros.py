import re
import abc
from collections import Sequence
from typing import Optional

from flask import Request, render_template, session, redirect, url_for, flash
from flask_login import current_user

from app.models import Usuario
from app import create_app
from ew20192_web.viewhelpers import ErrorNotFoundViewHelper


app = create_app()
app.app_context().push()


class Filtro(abc.ABC):
    """[summary]
    """

    @abc.abstractmethod
    def set_next(self, filtro: "Filtro") -> "Filtro":
        """[summary]
        """

    @abc.abstractmethod
    def do_chain(self, request: Request) -> Optional[str]:
        """[summary]
        """


class AbstractFiltro(Filtro):
    """[summary]
    """

    _next_filtro: Filtro = None

    def set_next(self, filtro: Filtro) -> Filtro:
        self._next_filtro = filtro
        return filtro

    def do_chain(self, request: Request) -> str:
        if self._next_filtro:
            return self._next_filtro.do_chain(request)


# dicionario de filtros para mapeamento dos filtros de interceptação
mapa_filtros = {}


def _mapeamento_filtro_por_rotas(nome_recurso: str = "/*", excluir_rotas: list = []):
    """TODO:[summary]

    Keyword Arguments:
        nome_recurso {str} -- [description] (default: {'/*'})

    Returns:
        [type] -- [description]
    """

    excluir_rotas = (list(excluir_rotas)
                     if isinstance(excluir_rotas, Sequence)
                     else list([excluir_rotas])
                     )

    def wrapper(cls: Filtro):
        # Adiciona a regra_func a lista indexada pela operacao, por meio da concatenacao de lista,
        # e atribui a lista concatenada ao dict chave operacao
        filtro_atual = cls()

        todos_recursos = (
            ([r.rule for r in list(app.url_map.iter_rules()) if "static" not in r.rule])
            if nome_recurso == "/*"else [nome_recurso])

        recursos = set(todos_recursos) - set(excluir_rotas)

        for recurso in recursos:
            # adiciona uma lista vazia indexado pelo nome do recurso
            # em maiusculo se ainda não existir a chave recurso
            mapa_filtros.setdefault(recurso, [])

            if len(mapa_filtros[recurso]) > 0:
                filtro_anterior: Filtro = mapa_filtros[recurso][-1]
                filtro_anterior.set_next(filtro_atual)
                mapa_filtros[recurso] = mapa_filtros[recurso] + [filtro_atual]
            else:
                mapa_filtros[recurso] = mapa_filtros[recurso] + [filtro_atual]

        return cls

    return wrapper


regra_geral_login = ['/', "/login", "/index", "/logout", "/cart",
                     "/categories", "/blog", "/product", "/contact",
                     '/checkout', "/admin/login", '/admin/logout','/teste',
                     '/admin/register', '/admin/forgot_password']

regra_papel_admin = []
regra_papel_usuario = []

@_mapeamento_filtro_por_rotas(excluir_rotas=regra_geral_login)
class Autenticacao(AbstractFiltro):
    def do_chain(self, request: Request):
        if not current_user.is_authenticated:
            uri = request.path
            return redirect(url_for("bp_admin.login", next=uri))
        return super().do_chain(request)


@_mapeamento_filtro_por_rotas(excluir_rotas=regra_geral_login)
class Autorizacao(AbstractFiltro):
    def do_chain(self, request: Request):
        if current_user.perfil != 'admin':
            uri = request.path
            flash(f'Atenção! Você não tem permissão para acessar a página {uri}')
            return redirect(url_for('bp_admin.login', next=uri))            
        return super().do_chain(request)



@_mapeamento_filtro_por_rotas(None)
class FiltroDefault(AbstractFiltro):
    def do_chain(self, request):
        return super().do_chain(request)
