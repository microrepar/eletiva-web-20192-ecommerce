from flask import Request, request


from ew20192_core.aplicacao import Resultado
from ew20192_core.utils import get_parameters_request
from ew20192_web.command import commands
from ew20192_web.viewhelpers import AbstractViewHelper, viewhelpers


class Controle:
    """Controla o fluxo de todas as requisitções efetuadas para o sistema
    """
    def __init__(self):
        self.commands = commands
        self.viewhelpers = viewhelpers
        
        
    def __call__(self):
        """Recebe a requisição e despacha para a viewhelper e para a command, respectivamente
        mapeadas pelo recurso e pela operacao que foram enviados no objeto request
        
        Arguments:
            request {Request} -- contém dados da requisição http enviada pelo cliente
        
        Returns:
            [Template jinja2] - - template html renderizado com jinja2 retornado pela viewhelper.
        """
        # Normaliza a chamada dos dicionários de parâmetros get e post 
        parameters = get_parameters_request(request)
       
        # Armazena o recurso solicitado pela requisição   
        uri: str = request.path
        # Recupera a classe viewhelper indexada mapeada pela uri e cria uma instância
        vh: AbstractViewHelper = self.viewhelpers.get(uri,self.viewhelpers.get(None))()
        
        # Constroi o objeto entidade com os dados enviados pela request
        entidade = vh.get_entidade(request)

        # Armazena a operação solicitada na requisição
        operacao: str = parameters.get('operacao')
                
        # Recupera função command para executar a operacao solicitada
        command = self.commands.get(operacao, self.commands[None])
        
        print('>>>> CONTROLE OPERACAO:', operacao)
        print('>>>>', str(uri) + ':', vh.__class__.__name__, '->', entidade.__class__.__name__,
            '\n>>>>', str(operacao) +':', command.__name__)

        # Executa a função command e envia o argumento entidade e devolve um objeto Resultado
        resultado: Resultado = command(entidade)

        # Executa o metodo enviando como argumento o resutado e a request para responder à requisição
        return vh.set_view(resultado, request)
