#!/usr/bin/env python

"""
Flask SECRET_KEY generator.
"""
import random
import uuid

chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'

CONFIG_STRING = """
FLASK_APP=eletiva_web_ecommerce.py
SECRET_KEY=%s
# DEBUG=False
# SQLALCHEMY_DATABASE_URI= 
# SQLALCHEMY_TRACK_MODIFICATIONS=False
# LOG_TO_STDOUT=
# MAIL_SERVER=
# MAIL_PORT=
# MAIL_USE_TLS=
# MAIL_USERNAME=
# MAIL_PASSWORD=
# ADMINS= ['your-email@example.com']
# LANGUAGES= ['en', 'es']
# MS_TRANSLATOR_KEY=
# ELASTICSEARCH_URL=
# REDIS_URL=
# POSTS_PER_PAGE=
""".strip() % ''.join(random.choice(chars) for _ in range(50))

# Writing our configuration file to '.env'
with open('.env', 'w') as configfile:
    configfile.write(CONFIG_STRING)
