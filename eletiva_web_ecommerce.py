from app import db, create_app
from flask import Blueprint
from app.models import Usuario

app = create_app()

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Usuario': Usuario}


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
