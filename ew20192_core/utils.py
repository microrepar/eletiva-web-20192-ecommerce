"""Funções e classe utilitarias
"""
import datetime
from itertools import zip_longest
from typing import Dict, List

from flask import Request


def get_parameters_request(request: Request) -> "ImutableDict":
    if request.method == "GET":
        return request.args
    return request.form


class ConverteData:

    @classmethod
    def stringDate(cls, strData: str):
        if strData is None or strData.strip() == '':
            return None
        try:
            data = datetime.datetime.strptime(strData, '%d/%m/%Y')
        except Exception as e:
            str(e)
            return None
        else:
            return data

    @classmethod
    def dateString(cls, dtData):
        if dtData is not None:
            return dtData.strftime('%d/%m/%Y')


class ConverteHora:

    @classmethod
    def stringHora(cls, strDateTime: str):
        if strDateTime is None or strDateTime.strip() == '':
            return None
        try:
            hora = datetime.datetime.strptime(strDateTime, '%H:%M')
        except Exception as e:
            str(e)
            return None
        else:
            return hora

    @classmethod
    def stringDataHora(cls, strDate: str, strTime):
        if not all([strDate, strTime, strDate.strip(), strTime.strip()]):
            return None
        try:
            data = datetime.datetime.strptime(strDate, '%d/%m/%Y')
            hora = datetime.datetime.strptime(strTime, '%H:%M')
            data_hora = datetime.datetime.combine(data.date(), hora.time())
        except Exception as e:
            str(e)
            return None
        else:
            return data_hora

    @classmethod
    def datetimeStringHora(cls, dthString: datetime.datetime):

        try:
            hora = dthString.strftime('%H:%M')
        except Exception as e:
            str(e)
            return None
        else:
            return hora


def _path(): ...  # TODO: implementar a verificacao do tamanho do path para pegar substring


_conversores = {'<int': int, '<float': float, 'path': _path}


def get_chain_of_filters(uri: str, mapa_filtros: Dict[str, List["Filtro"]]):
    filtros = mapa_filtros.get(uri, None)
    if filtros:
        return filtros[0]

    split_uri = uri.split('/')

    for key, value in mapa_filtros.items():
        if key is None:
            continue
        split_key = key.split('/')
        for piece_uri, piece_key in zip_longest(split_uri, split_key, fillvalue=None):
            if _tem_conversor(piece_key) and _conversor_compativel(piece_uri, _qual_conversor(piece_key)):
                continue

            if piece_uri != piece_key and _qual_conversor(piece_key) is None:
                filtro_padrao = mapa_filtros.get(None)[0]
                return filtro_padrao
        else:
            filtro = value[0]
            return filtro


def _tem_conversor(conversor: str) -> bool:
    if '<' in conversor and '>' in conversor:
        return True
    return False


def _qual_conversor(conversor: str) -> str:
    for tipo in _conversores.keys():
        if tipo in conversor:
            return tipo


def _conversor_compativel(valor: str, tipo_conversor: str) -> bool:
    try:
        _conversores.get(tipo_conversor, str)(valor)
        return True
    except AttributeError:
        return False
